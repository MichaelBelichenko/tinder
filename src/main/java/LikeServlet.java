import entity.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class LikeServlet extends HttpServlet{

    DB db = new DB();

    List<User> likedUsers = new ArrayList<>();

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");

        db.addLikedUser(name);

        resp.sendRedirect("/main");
    }



}
