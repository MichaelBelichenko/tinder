

import static freemarker.template.Configuration.VERSION_2_3_21;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

import entity.User;
import templater.PageGenerator;

public class MainServlet extends HttpServlet {

    DB db = new DB();

    int a = db.trunkate();

    List<User> users =  db.getAll();



    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Random random = new Random();
        int index = random.nextInt(users.size());
        User user = users.get(index);

        PageGenerator pageGenerator = PageGenerator.instance();
        Map<String, Object> map = new HashMap<>();
        map.put("user", user);
        String page = pageGenerator.getPage("index.html", map);

        resp.getWriter().write(page);
    }



}
