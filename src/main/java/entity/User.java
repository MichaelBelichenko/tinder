package entity;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class User {

    private int id;
    private String name;
    private String imgURL;
    private ArrayList<User> likedUsers;
    private String pass;
    private int age;
    private String email;

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", imgURL='" + imgURL + '\'' +
                ", likedUsers=" + likedUsers +
                ", pass='" + pass + '\'' +
                ", age=" + age +
                ", email='" + email + '\'' +
                '}';
    }



    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public ArrayList<User> getLikedUsers() {
        return likedUsers;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImgURL() {
        return imgURL;
    }

    public void setImgURL(String imgURL) {
        this.imgURL = imgURL;
    }
}