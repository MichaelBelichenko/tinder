import entity.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DB {

    public List<User> getAll() {
        List<User> userList = new ArrayList<>();
        Connection connection = getConnection();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM tinder_USERS;");

            UserMapper userMapper = new UserMapper();

            while (resultSet.next()) {

                User product = userMapper.mapRow(resultSet);

                userList.add(product);

            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userList;
    }

    public int trunkate() {
        Connection connection = getConnection();
        try {
            Statement statement = connection.createStatement();
            int resultSet = statement.executeUpdate("TRUNCATE TABLE maikl_liked;");

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 1;

    }

    public int addLikedUser(String name){
        Connection connection = getConnection();
        try {
            Statement statement = connection.createStatement();
            int resultSet = statement.executeUpdate("INSERT INTO maikl_liked (name) VALUES ('"+name+"');");

        } catch (SQLException e) {
            e.printStackTrace();
        } return 1;
    }


    private Connection getConnection() {
        String url = "jdbc:mysql://danit.cukm9c6zpjo8.us-west-2.rds.amazonaws.com:3306/fs3";
        String user = "fs3_user";
        String password = "bostoN";

        // create connection
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(url, user, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

}
