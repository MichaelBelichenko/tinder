import entity.User;



import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "mainPage", loadOnStartup = 1, urlPatterns = "/likedusers")
public class UserServlet extends HttpServlet {
    DB db = new DB();


    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {

        response.getWriter().write(buildPage());
    }

    private String buildPage() {
        List<User> products = db.getAll();

        StringBuilder result = new StringBuilder();
        result.append("<html><body><ul>");

        for (User p : products) {
            result.append("<li>")
                    .append(p.getName())
                    .append("<br>")
                    .append(p.getAge())
                    .append("<br>")
                    .append(p.getEmail())
                    .append("<br>")
                    .append("<form method='POST' action='/product/cart'>")
                    .append("<input type='hidden' name='id' value='")
                    .append(p.getId())
                    .append("'/>")
                    .append("<button type='submit'>Add to Cart</button>")
                    .append("</form></li>");
        }
        result.append("</ul>");
        result.append(" </body></html>");
        return result.toString();
    }

}
