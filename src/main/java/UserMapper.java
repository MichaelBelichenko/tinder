import entity.User;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserMapper {
    public User mapRow(ResultSet resultSet) throws SQLException {

        User user = new User();

        int id = resultSet.getInt("id");
        String name = resultSet.getString("name");
        int age = resultSet.getInt("age");
        String img = resultSet.getString("img");
        String email = resultSet.getString("email");
        String pass = resultSet.getString("pass");
        user.setId(id);
        user.setName(name);
        user.setAge(age);
        user.setImgURL(img);
        user.setPass(pass);


        // работаем с resultSet, достаем нужные нам поля по колонкам и вкладываем значения в объект User
        return user;
    }
}