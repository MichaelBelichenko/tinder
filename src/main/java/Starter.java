
import entity.User;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import java.util.Deque;


public class Starter {




    public static void main(String[] args) throws Exception {
        Server server = new Server(8080);


        ServletContextHandler handler = new ServletContextHandler();
        ServletHolder holder = new ServletHolder(new MainServlet());
        handler.addServlet(holder, "/main");

        ServletHolder freemarkerHolder = new ServletHolder(new UserServlet());
        handler.addServlet(freemarkerHolder, "/users");

        ServletHolder like = new ServletHolder(new LikeServlet());
        handler.addServlet(like, "/like");

        server.setHandler(handler);
        server.start();

        server.join();
    }
}
